<?php
session_start();

require_once(__DIR__ . '/components/head.php');

if ($_SESSION['user']->admin == false) {
  header("Location: login");
  session_destroy();
  die;
}
?>

<body id="admin-panel" class="admin-panel">
  <?php
    require_once(__DIR__ . '/components/nav.php');
   ?>
  <section id="admin-top" class="sub-top-section">
    <div class="container">
      <h1>Admin panel</h1><br><br>
      <a class="btn primary-button" href="admin_panel_orders">Go to orders</a>
    </div>
  </section>
  <section class="admin-products">
    <div class="container">
      <div class="section-title">
        <h2>Products list</h2>
        <a id="show-form-one" class="btn primary-button add">Create product</a>
      </div>
      <?php
      require_once(__DIR__ . '/APIs/api-get-products.php');

      foreach ($_products as $product) : ?>

        <div class="product-item">
          <div class="product-left">
            <a href="product?product_id=<?= $product->product_id ?>">
              <div class="product-image">
                <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
              </div>
            </a>
            <div class="product-info">
              <div class="title">
                <?= $product->product_name ?>
              </div>
              <div class="price"><?= $product->product_price ?> DKK.</div>
            </div>
          </div>
          <div class="product-right">
            <form id="product-<?=  $product->product_id ?>" class="remove-product-form" method="post">
              <input name="product_id" type="hidden" value="<?= $product->product_id ?>">
              <button class="btn remove" tabindex=""></button>
            </form>
          </div>
        </div>

      <?php
      endforeach; ?>
    </div>
  </section>
  <div id="popupOne" class="popup" data-theme="">
      <section class="indhold">
        <button class="luk"></button>
        <div class="form-wrapper">
            <h2>Create a product</h2>
            <form id="product-form" class="form" method="POST">
                <label for="product_name">Product title</label>
                <input name="product_name" type="text" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
                <label for="product_price">Price</label>
                <input name="product_price" type="text" data-validate="str" data-min="2" data-max="10" tabindex="2" required>
                <label for="product_description_short">Product short description</label>
                <textarea name="product_description_short" type="text" data-validate="str" data-min="2" data-max="150" tabindex="3" required></textarea>
                <label for="product_description_long">Product long description</label>
                <textarea name="product_description_long" type="text" data-validate="str" data-min="2" data-max="150" tabindex="4" required></textarea>
                <label for="product_category_id">Product category id</label>
                <input name="product_category_id" type="text" data-validate="str" data-min="2" data-max="10" tabindex="5" required>
                <label for="product_image">Attach image</label>
                <input name="product_image" type="file" required tabindex="6">
                <button class="btn primary-button" tabindex="6">Upload product</button>
            </form>
        </div>
      </section>
  </div>
  <section class="admin-categories">
    <div class="container">
      <div class="section-title">
        <h2>Category list</h2>
        <a id="show-form-two" class="btn primary-button add">Create category</a>
      </div>
      <?php
      require_once(__DIR__ . '/APIs/api-get-categories.php');

      foreach ($_categories as $item) : ?>

        <div class="category-item">
          <div class="category-left">
            <a href="shop?category_id=<?= $item->category_id ?>">
              <img src="/webshop/assets/images/category_images/<?= $item->image_file ?>" alt="<?= $item->category_name ?>">
            </a>
            <div class="item-title">
              <div class="title"><?= $item->category_name ?></div>
              <div class="">Id: <?= $item->category_id ?></div>
            </div>
          </div>
          <form id="cat-id-<?=  $item->category_id ?>" class="remove-category-form" method="post">
            <input name="category_id" type="hidden" value="<?= $item->category_id ?>">
            <button class="btn remove" tabindex=""></button>
          </form>
        </div>

      <?php
      endforeach; ?>
    </div>
  </section>
  <div id="popupTwo" class="popup" data-theme="">
      <section class="indhold">
        <button class="luk"></button>
        <div class="form-wrapper">
            <h2>Create a category</h2>
            <form id="category-form" class="form">
                <label for="category_name">Category name</label>
                <input name="category_name" type="text" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
                <label for="category_description">Category description</label>
                <textarea name="category_description" type="text" data-validate="str" data-min="2" data-max="150" tabindex="3" required></textarea>
                <label for="image_file">Attach image</label>
                <input name="image_file" type="file" required tabindex="5">
                <button class="btn primary-button" tabindex="5">Upload item</button>
            </form>
        </div>
      </section>
  </div>
  <section class="admin-categories">
    <div class="container">
      <div class="section-title">
        <h2>Active users</h2>
      </div>
      <?php
      require_once(__DIR__ . '/APIs/api-get-users.php');

      foreach ($_admin_info_users as $item) : ?>

        <div class="category-item">
          <div class="category-left">
            <div class="item-title">
              <div class="title"><?= $item->user_name . " " . $item->last_name ?></div>
              <div class=""><?= $item->user_email ?></div>
              <div class="">Id: <?= $item->user_id ?></div>
            </div>
          </div>
          <form id="cat-id-<?=  $item->category_id ?>" class="remove-category-form" method="post">
            <input name="category_id" type="hidden" value="<?= $item->category_id ?>">
            <!-- <button class="btn remove" tabindex=""></button> -->
          </form>
        </div>

      <?php
      endforeach; ?>
    </div>
  </section>
  <?php
  require_once(__DIR__ . '/components/footer.php');
  ?>
</body>
