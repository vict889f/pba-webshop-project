<?php
session_start();
if ($_SESSION['user'] == false) {
  header("Location: login");
  session_destroy();
  die;
}
require(__DIR__ . '/components/head.php');
require_once(__DIR__ . '/APIs/api-get-users.php');
?>

<body id="profile-page" class="profile-page">
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section class="sub-top-section">
    <div class="container">
      <h1>Profile page</h1>
    </div>
  </section>
  <section class="content-section profile-info">
    <div class="user-info">
          <div class="section-title">
            <h3>Your information</h3>
            <div class="section-button">
              <a id="show-form-one" class="btn primary-button edit">Edit</a>
            </div>
          </div>
        <div class="">
          <?= $_user->user_name . " " . $_user->last_name ?>
          <br>
          <?= $_user->user_email ?>
        </div>
    </div>
  </section>
  <section class="content-section profile-info">
    <div class="user-info">
          <div class="section-title">
            <h3>Shipment and payment information</h3>
            <div class="section-button">
              <a id="show-form-two" class="btn primary-button edit">Edit</a>
            </div>
          </div>
        <div class="">
          <?= $_user->user_name . " " . $_user->last_name ?>
          <br>
          <?php if ($_user_information): ?>
            <?= $_user_information->address ?>
            <br>
            <?= $_user_information->city ?>
            <br>
            <?= $_user_information->zip ?>
            <br>
            <?= $_user_information->country ?>
          <?php endif; ?>

        </div>
    </div>
  </section>
  <section id="cart" class="content-section profile-info">
    <div class="user-info">
      <div class="section-title">
        <h3>Your order history</h3>
      </div>
      <div class="products-container">
        <?php
        require_once(__DIR__ . '/APIs/api-get-orders.php');
        require_once(__DIR__ . '/APIs/api-get-products.php');

        foreach ($_orders as $order) : ?>
          <?php if ($order->user_id == $_SESSION['user']->user_id): ?>
            <div class="order-title">
              <h4>Order id: <?= $order->order_id ?></h4>
            </div>
            <div class="">
              <?php foreach ($_order_items as $order_item): ?>
                <?php if ($order_item->order_id == $order->order_id): ?>
                  <div class="single_order_item">
                    <div class="single-product-inner">
                    <?php foreach ($_products as $product): ?>
                      <?php if ($product->product_id == $order_item->product_id): ?>
                        <div class="product-item in-cart">
                          <div class="product-info">
                            <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
                            <div class="title">
                              <?= $product->product_name ?>
                            </div>
                          </div>
                          <div class="product-right">
                            <div class="price"><?= $product->product_price ?> DKK.</div>
                          </div>
                        </div>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </div>
                <?php endif; ?>
              <?php endforeach; ?>
              </div>
              <div class="total-price">
                <div class="amount">Total <?= $order->total_price ?> DKK</div>
              </div>
              <hr><br><br>
              <?php else: ?>
              <p></p>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    </div>
  </section>

  <div id="popupOne" class="popup" data-theme="">
      <section class="indhold">
        <button class="luk"></button>
        <div class="form-wrapper">
            <h2>Update account</h2>
            <form id="user-form" class="form" method="post">
                <input name="user_id" type="hidden" value="<?= $_user->user_id ?>">
                <label for="user_name">First name</label>
                <input name="user_name" type="text" placeholder="name" value="<?= $_user->user_name ?>" data-validate="str" data-min="2" data-max="20" tabindex="1" required>
                <label for="last_name">Last name</label>
                <input name="last_name" type="text" placeholder="lastname" value="<?= $_user->last_name ?>" data-validate="str" data-min="2" data-max="20" tabindex="2" required>
                <label for="email">Email</label>
                <input name="email" type="text" placeholder="email" value="<?= $_user->user_email ?>" data-validate="email" tabindex="3" required>
                <label for="password">Password</label>
                <input name="password" type="password" placeholder="password" value="" data-validate="str" data-min="6" data-max="20" tabindex="4" required>
                <label for="password">Password</label>
                <input name="password" type="password" placeholder="password" value="" data-match-name="password" data-validate="match" tabindex="5">
                <br>
                <button class="btn primary-button" tabindex="6">Save changes</button>
            </form>
        </div>
      </section>
  </div>
  <div id="popupTwo" class="popup" data-theme="">
      <section class="indhold">
        <button class="luk"></button>
        <div class="form-wrapper">
            <h2>Shipping and payment information</h2>
            <form id="<?= ($_user_information) ? 'update-info-form' : 'user-info-form'; ?>" class="form user-info-form" method="POST">
                <input name="user_id" type="hidden" value="<?= $_user->user_id ?>">
                <label for="address">Address</label>
                <input type="text" name="address" value="<?= ($_user_information) ? $_user_information->address : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="1" required>
                <label for="city">City</label>
                <input type="text" name="city" value="<?= ($_user_information) ? $_user_information->city : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="2" required>
                <label for="zip">Zip code</label>
                <input type="text" name="zip" value="<?= ($_user_information) ? $_user_information->zip : ''; ?>" data-validate="str" tabindex="3" required>
                <label for="country">Country</label>
                <input type="text" name="country" value="<?= ($_user_information) ? $_user_information->country : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="2" required>
                <label for="preferred_payment">Preferred payment method</label>
                <div class="radio-btn">
                  <input type="radio" id="paypall" name="payment" value="1" <?= ($_user_information) ? (($_user_information->preferred_payment == 1) ? 'checked' : '') : ''; ?>>
                  <label for="paypall">Paypall</label>
                </div>
                <div class="radio-btn">
                  <input type="radio" id="mobilepay" name="payment" value="2" <?= ($_user_information) ? (($_user_information->preferred_payment == 2) ? 'checked' : '') : ''; ?>>
                  <label for="mobilepay">MobilePay</label>
                </div>
                <div class="radio-btn">
                  <input type="radio" id="card" name="payment" value="3" <?= ($_user_information) ? (($_user_information->preferred_payment == 3) ? 'checked' : '') : ''; ?>>
                  <label for="card">Card</label>
                </div>
                <br>
                <button class="btn primary-button" tabindex="6">Save changes</button>
            </form>
        </div>
      </section>
  </div>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
