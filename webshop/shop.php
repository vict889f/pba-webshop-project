<?php
session_start();

require(__DIR__ . '/components/head.php');

require(__DIR__ . '/APIs/api-get-products.php');

?>

<body class="shop<?= ($_SESSION["user"]->admin == 1) ? ' admin' : ''; ?>">
  <?php require(__DIR__ . '/components/nav.php'); ?>

  <section class="sub-top-section">
    <div class="container">
      <h1>Shop</h1>
    </div>
  </section>
  <section id="products">
    <div class="container">
      <div class="section-title">
        <?php if (!empty($category_id)) : ?>
          <div class="title">
            <h2>Showing category: <?= $_category->category_name ?></h2>
            <?php if (!empty($_SESSION["user"]) && $_SESSION["user"]->admin == 1): ?>
              <a id="show-form" class="btn primary-button edit">Edit category</a>
            <?php endif; ?>
          </div>
          <div class="buttons">
            <a class="btn secondary-button" href="shop">All products</a>
          </div>
        <?php else : ?>
          <div class="title">
            <h2>All products</h2>
          </div>
        <?php endif; ?>
      </div>
      <div class="products-container">
        <?php
        foreach ($_products as $product) : ?>

        <div class="product-item">
          <a href="product?product_id=<?= $product->product_id ?>">
            <form id="product-<?=  $product->product_id ?>" class="product-form" method="post">
              <input name="product_id" type="hidden" value="<?= $product->product_id ?>">
              <div class="product-image">
                <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
              </div>
              <div class="product-info">
                <div class="title">
                  <?= $product->product_name ?>
                </div>
                <div class="price"><?= $product->product_price ?> DKK.</div>
              </div>
            </a>
            <button class="btn add-to-cart">Add to cart</button>
          </form>
        </div>

        <?php
        endforeach;
        ?>
      </div>
    </div>
  </section>
  <?php if (!empty($_SESSION["user"]) && $_SESSION["user"]->admin == 1): ?>
    <div id="popupOne" class="popup" data-theme="">
        <section class="indhold">
          <button class="luk"></button>
          <div class="form-wrapper">
            <h2>Edit category</h2>
            <form id="update-category-form" class="form">
              <input name="category_id" type="hidden" value="<?= $_category->category_id ?>" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
              <label for="category_name">Category name</label>
              <input name="category_name" type="text" value="<?= $_category->category_name ?>" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
              <label for="category_description">Category description</label>
              <textarea name="category_description" type="text" data-validate="str" data-min="2" data-max="150" tabindex="3" required><?= $_category->category_description ?></textarea>
              <label for="image_file">Attach image</label>
              <input name="image_file" type="file" required tabindex="5">
              <button class="btn primary-button" tabindex="5">Update category</button>
            </form>
          </div>
        </section>
    </div>
  <?php endif; ?>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
