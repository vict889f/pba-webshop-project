<?php

function totalPrice() {
  $total_price = 0;
  if ( !empty($_SESSION['cart']) ){
    foreach ($_SESSION['cart'] as $product) {
      $price = $product->product_price;
      $total_price += $price;
    }
  }
  if ( empty($_SESSION['cart']) && !empty($_SESSION['order']) ) {
    foreach ($_SESSION['order'] as $order) {

      foreach ( $order as $product) {
        $price = $product->product_price;
        $total_price += $price;
        // $products = print_r($product->product_price);
      }
    }
  }
  return $total_price;
  // return $order;
}

function cartQuantity() {
  $cart_qty = 0;
  foreach ($_SESSION['cart'] as $product) {
    $cart_qty = $cart_qty + 1;
  }
  return $cart_qty;
}
