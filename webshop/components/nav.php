<header>
  <div id="nav" class="nav-bar">
    <div class="container">
      <div class="nav-bar-inner">
          <div class="logo-title">
              <a href="index">
                <img src="assets/images/icons/chilli-logo.png" alt="logo">
                <span>Webshop</span>
              </a>
          </div>
          <!-- <nav class="navigation">
              <a href="shop">Shop</a>
              <a href="on_sale">Sale</a>
              <a href="about">About</a>
          </nav> -->
        <div class="account">
          <?php if (isset($_SESSION["user"])) { ?>
            <?php if ($_SESSION["user"]->admin == 1) { ?>
                <a href='admin_panel'>Admin</a>
            <?php };?>
              <a href="profile?id=<?php echo $_SESSION['user']->user_id; ?>">Profile</a>
              <!-- <a href="#">User id: <?php echo $_SESSION['user']->user_name; ?> </a> -->
              <a href="APIs/api-destroy">Log out</a>
          <?php } else { ?>
              <a href='login'>Login</a>
          <?php
          }
          require(__DIR__ . '/functions.php');
          ?>
          <div class="basket-amount">
            <a href="cart">
              <?php if (!empty($_SESSION['cart'])) { ?>
              <div class="amount"><?php echo totalPrice(); ?> DKK</div>
              <?php } ?>
              <div class="cart-icon">
                <div class="cart"></div>
                <?php if (!empty($_SESSION['cart'])) { ?>
                  <div class="cart-qty"><?php echo cartQuantity(); ?></div>
                <?php } ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
