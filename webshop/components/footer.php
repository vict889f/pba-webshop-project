<footer>
  <div class="container">

    <div class="footer-inner">
      <div class="logo-title">
          <a href="index">
            <img src="assets/images/icons/logo-large.png" alt="logo">
            <span>Webshop</span>
          </a>
      </div>
      <div class="footer-nav">
        <div class="nav-item">
          <div class="title">
            Contact us
          </div>
          <div class="adress">
            Adress 123, <br>
            2100 københavn N
          </div>
          <br>
          <div class="info">
            <a href="#">service@webshop.com</a><br>
            <a href="#">Tlf. 31 42 55 66</a>
          </div>
        </div>
        <div class="nav-item">
          <div class="title">
            Links
          </div>
          <div class="links">
            <div class="link-item">
              <a href="APIs/api-destroy">Destroy session</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</footer>
<script src="/webshop/assets/scripts/script.js"></script>
<script src="/webshop/assets/scripts/validator.js"></script>
<script src="/webshop/assets/scripts/APIs.js"></script>

</html>
