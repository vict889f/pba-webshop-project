<?php
session_start();

require(__DIR__ . '/components/head.php');

?>

<body class="">
  <?php

  // require(__DIR__ . '/components/nav.php');

  ?>
    <header>
      <div id="nav" class="nav-bar">
        <div class="container">
          <div class="nav-bar-inner">
            <div class="logo-title">
                <a href="index">
                  <img src="assets/icons/chilli-logo.png" alt="logo">
                  <span>Webshop</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="form-section">
        <div class="form-wrapper">
            <h2>Thank you for signing up</h2>
            <h3>Please check your email to verify your account</h3>
            <br>
        </div>
    </section>
</body>

<?php

require(__DIR__ . '/components/footer.php');
