<?php

require(__DIR__ . '/components/head.php');

?>

<body>
  <?php

  require(__DIR__ . '/components/nav.php');

  ?>
<body>
    <section class="form-section">
        <div class="form-wrapper">
            <h2>Your email has been validated</h2>
            <br>
            <a href="login"><span class="sign-in-button">Continue</span></a>
        </div>
    </section>
    <?php
    require(__DIR__ . '/components/footer.php');
    ?>
</body>
