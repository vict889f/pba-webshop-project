<?php
$key = $_GET["key"];

require(__DIR__ . '/components/head.php');
?>

<body class="change-password">
  <section class="form-section">
      <div class="form-wrapper">
          <h2>Type in your new password</h2>
          <form id="new-password-form" class="form" method="POST">
              <input name="key" type="hidden" value="<?= $key ?>">
              <label for="password">Password</label>
              <input name="password" type="password" placeholder="min. 6 characters" data-validate="str" data-min="6" data-max="20" tabindex="1">
              <label for="password">Re-enter password</label>
              <input name="re_password" type="password" data-match-name="password" data-validate="match" tabindex="2">
              <button class="btn primary-button">Change password</button>
          </form>
      </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
