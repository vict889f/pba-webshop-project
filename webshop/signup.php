<?php
session_start();

require(__DIR__ . '/components/head.php');

?>

<body id="signup-page" class="signup-page">
  <?php
  require(__DIR__ . '/components/nav.php');

  ?>
    <section class="form-section">
        <div class="form-wrapper">
            <h2>Create account</h2>
            <form id="signup-form" class="form signup-form" method="POST" onsubmit="validate(signUp); return false">
                <label for="user_name">First name</label>
                <input type="text" name="user_name" data-validate="str" data-min="2" data-max="20" tabindex="1">
                <label for="last_name">Last name</label>
                <input type="text" name="last_name" data-validate="str" data-min="2" data-max="20" tabindex="2">
                <label for="email">Email</label>
                <input type="text" name="email" data-validate="email" tabindex="3" required>
                <label for="password">Password</label>
                <input type="password" name="password" placeholder="min. 6 characters" data-validate="str" data-min="6" data-max="20" tabindex="4">
                <label for="password_match">Re-enter password</label>
                <input type="password" name="password_match" data-match-name="password" data-validate="match" tabindex="5">
                <button class="btn primary-button" tabindex="6">Sign up</button>
            </form>
        </div>
    </section>
    <?php
    require(__DIR__ . '/components/footer.php');
    ?>
</body>
