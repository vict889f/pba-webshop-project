<?php
session_start();

require_once(__DIR__ . '/components/head.php');

if ($_SESSION['user']->admin == false) {
  header("Location: login");
  session_destroy();
  die;
}
?>

<body id="admin-panel" class="admin-panel">
  <?php
    require_once(__DIR__ . '/components/nav.php');
   ?>
  <section id="admin-top" class="sub-top-section">
    <div class="container">
      <h1>Admin panel: Orders</h1>
    </div>
  </section>
  <section class="admin-products">
    <div class="container">
      <div class="section-title">
        <h2>Orders list</h2>
      </div>
      <?php
      require_once(__DIR__ . '/APIs/api-get-orders.php');
      require_once(__DIR__ . '/APIs/api-get-products.php');
      require_once(__DIR__ . '/APIs/api-get-users.php');

      foreach ($_orders as $order) : ?>
      <div class="shop-order">
        <div class="title">
          <div class="">
              Order id: <?= $order->order_id ?>
          </div>
        </div>
        <div class="order_items">
          <?php foreach ($_order_items as $order_item): ?>
            <?php if ($order_item->order_id == $order->order_id): ?>
              <div class="single_order_item">
                <div class="single-product-inner">
                  <div class="product-id">
                      Product id: <?= $order_item->product_id ?>
                  </div>
                <?php foreach ($_products as $product): ?>
                  <?php if ($product->product_id == $order_item->product_id): ?>
                      <div class="name">
                        <?= $product->product_name ?>
                      </div>
                    </div>
                    <div class="price">
                      <?= $product->product_price ?> DKK.
                    </div>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
          </div>
          <div class="order-total">
            Order total: <?= $order->total_price ?> DKK.
          </div>
          <div class="order-recipient">
            Order recipient:
            <div class="customer">
              <?php foreach ($_admin_info_users as $user): ?>
                <?php if ($user->user_id == $order->user_id): ?>
                    <div class="title"><?= $user->user_name . " " . $user->last_name ?></div>
                    <div class=""><?= $user->user_email ?></div>
                    <div class="">Id: <?= $user->user_id ?></div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
          </div>

        </div>
        <hr>
      <?php endforeach; ?>
    </div>
  </section>
  <?php
  require_once(__DIR__ . '/components/footer.php');
  ?>
</body>
