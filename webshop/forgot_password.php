<?php

require(__DIR__ . '/components/head.php');

?>

<body class="forgot-password">
  <section class="form-section">
    <div class="form-wrapper">
      <h2>Type in your email to reset password</h2>
      <form id="forgot-password-form" class="form" method="POST">
          <label for="email">Email</label>
          <input name="email" type="text" data-validate="email" tabindex="1" required>
          <button class="btn primary-button">Send email</button>
      </form>
      <div class="message"></div>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
