<?php
session_start();

require(__DIR__ . '/components/head.php');

require_once(__DIR__ . '/APIs/api-get-products.php');

?>

<body id="single-product" class="single-product">
  <?php require(__DIR__ . '/components/nav.php'); ?>

  <section id="product">
    <div class="container">
      <div class="product-container">
        <div class="product-image">
          <img src="/webshop/assets/images/product_images/<?= $_product->product_image ?>" alt="<?= $_product->product_name ?>">
        </div>
        <div class="product-info-short">
          <h2><?= $_product->product_name ?></h2>
          <div class="price"><?= $_product->product_price ?> DKK.</div>
          <p><?= $_product->product_description_short ?></p>
          <br><br>
          <form id="product-<?=  $_product->product_id ?>" class="product-form" method="post">
            <input name="product_id" type="hidden" value="<?= $_product->product_id ?>">
            <button class="btn add-to-cart">Add to cart</button>
          </form>
          <?php if ( !empty($_SESSION['user']) && $_SESSION['user']->admin == true ): ?>
            <br><br>
            <a id="show-form" class="btn primary-button edit">Edit product</a>
          <?php endif; ?>
          </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="section-title">
        <h2>Product specifications</h2>
      </div>
      <div class="">
        <p><?= $_product->product_description_long ?></p>
      </div>
    </div>
  </section>
  <?php if ( !empty($_SESSION) && $_SESSION['user']->admin == true ): ?>
    <div id="popupOne" class="popup" data-theme="">
        <section class="indhold">
          <button class="luk"></button>
          <div class="form-wrapper">
              <h2>Update product</h2>
              <form id="update-product-form" class="form" method="POST">
                  <input name="product_id" type="hidden" value="<?= $_product->product_id ?>" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
                  <label for="product_name">Product title</label>
                  <input name="product_name" type="text" value="<?= $_product->product_name ?>" data-validate="str" data-min="2" data-max="50" tabindex="1" required>
                  <label for="product_price">Price</label>
                  <input name="product_price" type="text" value="<?= $_product->product_price ?>" data-validate="str" data-min="2" data-max="10" tabindex="2" required>
                  <label for="product_description_short">Product short description</label>
                  <textarea name="product_description_short" type="text" value="<?= $_product->product_description_short ?>" data-validate="str" data-min="2" data-max="150" tabindex="3" required><?= $_product->product_description_short ?></textarea>
                  <label for="product_description_long">Product long description</label>
                  <textarea name="product_description_long" type="text" value="<?= $_product->product_description_long ?>" data-validate="str" data-min="2" data-max="150" tabindex="4" required><?= $_product->product_description_long ?></textarea>
                  <label for="product_category_id">Product category id</label>
                  <input name="product_category_id" type="text" value="<?= $_product->product_category_id ?>" data-validate="str" data-min="2" data-max="10" tabindex="5" required>
                  <label for="product_image">Attach image</label>
                  <input name="product_image" type="file" value="<?= $_product->product_image ?>" required tabindex="6">
                  <button class="btn primary-button" tabindex="6">Update product</button>
              </form>
          </div>
        </section>
    </div>
  <?php endif; ?>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
