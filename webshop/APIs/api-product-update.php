<?php
require_once(__DIR__ . '/../private/globals.php');
session_start();

$statusMsg = '';

// File upload path
$targetDir = __DIR__ . '/../assets/images/product_images/';
$fileName = basename($_FILES["product_image"]["name"]);
$targetFilePath =  $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

// Validate
if (!isset($_POST['product_name'])) {
    _res(400, ['info' => 'item_name required', 'error' => __LINE__]);
}
if (strlen($_POST['product_name']) < _ITEM_MIN_LEN) {
    _res(400, ['info' => 'item_name min ' . _ITEM_MIN_LEN . ' characters', 'error' => __LINE__]);
}
if (strlen($_POST['product_name']) > _ITEM_MAX_LEN) {
    _res(400, ['info' => 'item_name min ' . _ITEM_MAX_LEN . ' characters', 'error' => __LINE__]);
}

if (!isset($_POST["submit"]) && empty($_FILES["product_image"]["name"])) {
    $statusMsg = 'Please select a file to upload.';
    _res(400, ['info' => 'Please select a file to upload.', 'error' => __LINE__]);
}

$allowTypes = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'webp');
if (!in_array(strtolower($fileType), $allowTypes)) {
    $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.';
    _res(400, ['info' => 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.', 'error' => __LINE__]);
}

// connect to DB
try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

if (move_uploaded_file($_FILES["product_image"]["tmp_name"], $targetFilePath)) {
  try {
      $product_id = $_POST['product_id'];
      $product_name = htmlspecialchars($_POST['product_name'], ENT_QUOTES, 'UTF-8');

      $q = $db->prepare('UPDATE products SET product_name = :product_name, product_price = :product_price, product_description_short = :product_description_short, product_description_long = :product_description_long, product_category_id = :product_category_id, product_image = :product_image
      WHERE product_id = :product_id');
      $q->bindValue(':product_id', $product_id);
      $q->bindValue(':product_name', $product_name);
      $q->bindValue(':product_price', $_POST['product_price']);
      $q->bindValue(':product_description_short', $_POST['product_description_short']);
      $q->bindValue(':product_description_long', $_POST['product_description_long']);
      $q->bindValue(':product_category_id', $_POST['product_category_id']);
      $q->bindValue(':product_image', $fileName);
      $q->execute();

      // $products = $q->fetch();

      _res(200, ['info' => 'Update success', 'error' => __LINE__]);
  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }
}
