<?php

require_once(__DIR__ . '/../private/globals.php');

// Validate
if (!isset($_POST['email'])) {
    _res(400, ['info' => 'email required']);
}
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    _res(400, ['info' => 'email is invalid']);
}

try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}

try {
    $email = $_POST['email'];

    $q = $db->prepare('SELECT * FROM users WHERE user_email = :user_email');
    $q->bindValue(':user_email', $email);
    $q->execute();
    $row = $q->fetch();

    if (!$row) {
        _res(400, ['info' => 'User doesnt exist', 'error' => __LINE__]);
    }
    if (!$row->verified_id) {
        _res(400, ['info' => 'User not verified', 'error' => __LINE__]);
    }

    sendEmail($row);

    _res(200, ['info' => 'Email sent']);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

function sendEmail($row) {
    $url = $_SERVER['HTTP_HOST'];
    $name = $row->user_name;
    $_to_email = $row->user_email;
    // $_to_email = "keawebdev2021@gmail.com";
    $_message = "Click this link to reset your password - <a href='http://$url/webshop/change_password.php?key=$row->forgot_password_key'>click here to change your password<a/>";
    require_once(__DIR__ . '/../private/send_email.php');
}
