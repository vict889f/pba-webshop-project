<?php

require_once(__DIR__ . '/../private/globals.php');


try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'test system under maintainance', 'error' => __LINE__]);
}

try {
    $q = $db->prepare('SELECT * FROM orders');
    $q->execute();

    $_orders = $q->fetchAll();

} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

try {
    $q = $db->prepare('SELECT * FROM order_items');
    $q->execute();

    $_order_items = $q->fetchAll();

} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
