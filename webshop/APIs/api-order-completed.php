<?php
session_start();

$user = $_SESSION['user'];

unset($_SESSION['order']);

$_SESSION['order'][] = $_SESSION["cart"];

//DODO: Compile order information in email message
$message = "";

sendEmail($user);

function sendEmail($user) {
    $name = $user->user_name;
    $_to_email = $user->user_email;
    // $_to_email = "keawebdev2021@gmail.com";
    $_message = "Thank you for your purchase!";
    require_once(__DIR__ . '/../private/send_email.php');
}

unset($_SESSION["cart"]);

header('Location: /../webshop/order_completed');
