<?php

require_once(__DIR__ . '/../private/globals.php');


try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'test system under maintainance', 'error' => __LINE__]);
}

$user_id = $_SESSION['user']->user_id;

try {

    $q = $db->prepare('SELECT * FROM users WHERE user_id = :user_id');
    $q->bindValue(':user_id', $user_id);


    $q->execute();
    $_user = $q->fetch();

    if (!$_user) {
        // _res(400, ['info' => 'could not get user info', 'error' => __LINE__]);
    }
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

try {

    $q = $db->prepare('SELECT * FROM user_information WHERE user_id = :user_id');
    $q->bindValue(':user_id', $user_id);


    $q->execute();
    $_user_information = $q->fetch();

    if (!$_user) {
        _res(400, ['info' => 'could not get user info', 'error' => __LINE__]);
    }
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

if ($_SESSION['user']->admin == true) {

  try {
      $q = $db->prepare('SELECT * FROM users WHERE verified_id = :verified_id');
      $q->bindValue(':verified_id', true);
      $q->execute();

      $_admin_info_users = $q->fetchAll();

  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }

}
