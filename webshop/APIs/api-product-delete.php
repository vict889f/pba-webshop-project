<?php
require_once(__DIR__ . '/../private/globals.php');

$db = _db();

try {
    $product_id = $_POST['product_id'];
    $q = $db->prepare('DELETE FROM products WHERE product_id = :product_id');
    $q->bindValue(':product_id', $product_id);
    $q->execute();

} catch (Exception $ex) {
  _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
