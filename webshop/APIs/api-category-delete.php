<?php
require_once(__DIR__ . '/../private/globals.php');

$db = _db();

$category_id = $_POST['category_id'];
echo $product_id;

try {

    $q = $db->prepare('DELETE FROM categories WHERE category_id = :category_id');
    $q->bindValue(':category_id', $category_id);
    $q->execute();
    // Success

} catch (Exception $ex) {
  _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
