<?php

require_once(__DIR__ . '/../private/globals.php');
// session_start();

try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'test system under maintainance', 'error' => __LINE__]);
}


try {
    $q = $db->prepare('SELECT * FROM categories');

    $q->execute();
    $_categories = $q->fetchAll();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
