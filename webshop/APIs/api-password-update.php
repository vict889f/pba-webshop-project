<?php
require_once(__DIR__ . '/../private/globals.php');

// connect to DB
try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

try {
    $key = $_POST['key'];

    $q = $db->prepare('SELECT * FROM users WHERE forgot_password_key = :old_key');
    $q->bindValue(':old_key', $key);
    $q->execute();
    $user = $q->fetch();

} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

try {
    $id = $user->user_id;
    $hashed_password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $forgot_password_key = bin2hex(random_bytes(8));

    $q = $db->prepare('UPDATE users SET user_password = :user_password, forgot_password_key = :forgot_password_key WHERE user_id = :id');
    $q->bindValue(':user_password', $hashed_password);
    $q->bindValue(':forgot_password_key', $forgot_password_key);
    $q->bindValue(':id', $id);
    $q->execute();


    _res(200, ['info' => 'Update success']);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
