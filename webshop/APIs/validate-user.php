<?php
require_once(__DIR__ . '/../private/globals.php');

if (!isset($_GET['key'])) {
    echo "Validation failed! (key is missing)";
    exit;
}
if (strlen($_GET['key']) != 32) {
    echo "Validation failed! (key is not 32 chars)";
    exit;
}


try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}

try {
    $key = $_GET['key'];
    $q = $db->prepare('SELECT * FROM users WHERE verification_key = :verification_key');
    $q->bindValue(':verification_key', $key);
    $q->execute();
    $row = $q->fetch();
    $user_id = $row->user_id;

    if (!$row) {
        _res(400, ['info' => 'Key doesnt exist in db', 'error' => __LINE__]);
    }

    // Mach in db
    validateUser($db, $user_id);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}

function validateUser($db, $user_id)
{
    try {
        $q = $db->prepare('UPDATE users SET verified_id = :verification_id WHERE user_id = :id');
        $q->bindValue(':verification_id', true);
        $q->bindValue(':id', $user_id);
        $q->execute();

        header('Location: /webshop/email_validated.php');
    } catch (Exception $ex) {
        _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    }
}
