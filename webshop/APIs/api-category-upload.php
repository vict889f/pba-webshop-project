<?php
require_once(__DIR__ . '/../private/globals.php');


$statusMsg = '';

// File upload path
$targetDir = __DIR__ . '/../assets/images/category_images/';
$fileName = basename($_FILES["image_file"]["name"]);
$targetFilePath =  $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);


// Validate
if (!isset($_POST["submit"]) && empty($_FILES["image_file"]["name"])) {
    $statusMsg = 'Please select a file to upload.';
    _res(400, ['info' => 'Please select a file to upload.', 'error' => __LINE__]);
}

$allowTypes = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'webp');
if (!in_array(strtolower($fileType), $allowTypes)) {
    $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.';
    _res(400, ['info' => 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.', 'error' => __LINE__]);
}


if (move_uploaded_file($_FILES["image_file"]["tmp_name"], $targetFilePath)) {
    $db = _db();
    try {
        $category_name = htmlspecialchars($_POST['category_name'], ENT_QUOTES, 'UTF-8'); # secured against script tags
        $category_description = htmlspecialchars($_POST['category_description'], ENT_QUOTES, 'UTF-8');

        $q = $db->prepare('INSERT into categories ( category_name, category_description, image_file )
                            VALUES ( :category_name, :category_description, :image_file )');
        $q->bindValue(':category_name', $category_name);
        $q->bindValue(':category_description', $category_description);
        $q->bindValue(':image_file', $fileName);
        $q->execute();

        // header('Location: /../index.php');
        _res(200, ['info' => 'Upload success']);
    } catch (Exception $ex) {
        $statusMsg = "File upload failed, please try again.";
        _res(500, ['info' => 'Error uploading file.', 'error' => __LINE__]);
    }
}
