<?php

require_once(__DIR__ . '/../private/globals.php');

// Validate
if (!isset($_POST['email'])) {
    _res(400, ['info' => 'email required']);
}
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    _res(400, ['info' => 'email is invalid']);
}

// Validate the password
if (!isset($_POST['password'])) {
    _res(400, ['info' => 'password required']);
}
if (strlen($_POST['password']) < _PASSWORD_MIN_LEN) {
    _res(400, ['info' => 'password min ' . _PASSWORD_MIN_LEN . ' characters']);
}
if (strlen($_POST['password']) > _PASSWORD_MAX_LEN) {
    _res(400, ['info' => 'password max ' . _PASSWORD_MAX_LEN . ' characters']);
}

try {
    $db = _db();
    // _res(200, ['info' => 'success connect db']);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}

// Check if the email exist in DB
try {
    $q = $db->prepare('SELECT user_email FROM users');
    $q->execute();

    $_user_emails = $q->fetchAll();

    foreach ($_user_emails as $email) {

        if ($_POST['email'] === $email->user_email) {
            _res(400, ['info' => 'email is already registerd']);
        }
    }
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}


try {
    $user_name = htmlspecialchars($_POST['user_name'], ENT_QUOTES, 'UTF-8');
    $user_last_name = htmlspecialchars($_POST['last_name'], ENT_QUOTES, 'UTF-8');
    $user_email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
    $hashed_password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $verification_key = bin2hex(random_bytes(16));
    $forgot_password_key = bin2hex(random_bytes(8));

    $q = $db->prepare('INSERT INTO users (user_name, last_name, user_email, user_password, verification_key, verified_id, forgot_password_key, admin)
    VALUES (:user_name, :last_name, :user_email, :user_password, :verification_key, :verified_id, :forgot_password_key, :admin)');
    $q->bindValue(':user_name', $user_name);
    $q->bindValue(':last_name', $user_last_name);
    $q->bindValue(':user_email', $user_email);
    $q->bindValue(':user_password', $hashed_password);
    $q->bindValue(':verification_key', $verification_key);
    $q->bindValue(':verified_id', 0);
    $q->bindValue(':forgot_password_key', $forgot_password_key);
    $q->bindValue(':admin', 0);
    $q->execute();

    // Pass information needed for verification email
    sendEmail($db, $user_name, $user_email);

} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

function sendEmail($db, $user_name, $user_email)
{
    try {
        $q = $db->prepare('SELECT * FROM users WHERE user_email = :user_email');
        $q->bindValue(':user_email', $user_email);
        $q->execute();
        $row = $q->fetch();

    } catch (Exception $ex) {
        _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    }

    $url = $_SERVER['HTTP_HOST'];
    $name = $user_name;
    $_to_email = $user_email;
    // $_to_email = "keawebdev2021@gmail.com";
    $_message = "Thank you for signing up! - Click this link to verify your email- <a href='http://$url/webshop/APIs/validate-user.php?key=$row->verification_key'>click here to verify<a/>";
    require_once(__DIR__ . '/../private/send_email.php');

    // header('Location: /../signup-ok');
}

// send ok for redirect
_res(200, ['info' => 'success signup - email sendt']);
