<?php

require_once(__DIR__ . '/../private/globals.php');
session_start();

// connect to DB
try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}


try {
    $id = $_SESSION['user']->user_id;
    $user_name = htmlspecialchars($_POST['user_name'], ENT_QUOTES, 'UTF-8');
    $user_last_name = htmlspecialchars($_POST['last_name'], ENT_QUOTES, 'UTF-8');
    $user_email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
    $hashed_password = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $q = $db->prepare('UPDATE users SET user_name = :user_name, last_name = :last_name, user_email = :user_email, user_password = :user_password WHERE user_id = :id');
    $q->bindValue(':user_name', $user_name);
    $q->bindValue(':last_name', $user_last_name);
    $q->bindValue(':user_email', $user_email);
    $q->bindValue(':user_password', $hashed_password);
    $q->bindValue(':id', $id);
    $q->execute();

    $user = $q->fetch();

    _res(200, ['info' => 'Update success']);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}
