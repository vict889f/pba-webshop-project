<?php

require_once(__DIR__ . '/../private/globals.php');

// Validate
if (!isset($_POST['address'])) {
    _res(400, ['info' => 'Address required']);
}

if (!isset($_POST['zip'])) {
    _res(400, ['info' => 'zip required']);
}

if (!isset($_POST['payment'])) {
    _res(400, ['info' => 'please select a payment']);
}


try {
    $db = _db();
    // _res(200, ['info' => 'success connect db']);
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}


try {
    $address = htmlspecialchars($_POST['address'], ENT_QUOTES, 'UTF-8');
    $city = htmlspecialchars($_POST['city'], ENT_QUOTES, 'UTF-8');
    $zip = htmlspecialchars($_POST['zip'], ENT_QUOTES, 'UTF-8');
    $country = htmlspecialchars($_POST['country'], ENT_QUOTES, 'UTF-8');
    $payment = $_POST['payment'];
    $user_id = $_POST['user_id'];

    $q = $db->prepare('INSERT INTO user_information (address, city, zip, country, preferred_payment, user_id)
    VALUES (:address, :city, :zip, :country, :preferred_payment, :user_id)');
    $q->bindValue(':address', $address);
    $q->bindValue(':city', $city);
    $q->bindValue(':zip', $zip);
    $q->bindValue(':country', $country);
    $q->bindValue(':preferred_payment', $payment);
    $q->bindValue(':user_id', $user_id);
    $q->execute();


} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

// send ok for redirect
_res(200, ['info' => 'information stored']);
