<?php
session_start();

require_once(__DIR__ . '/../private/globals.php');

try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    exit();
}

try {
    $user_id = $_POST['user_id'];
    $total_price = $_POST['total_price'];

    $q = $db->prepare('INSERT INTO orders (user_id, total_price, order_time)
    VALUES (:user_id, :total_price, NOW())');
    $q->bindValue(':user_id', $user_id);
    $q->bindValue(':total_price', $total_price);
    $q->execute();

    $_order_id = $db->lastInsertId();


} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

foreach ($_SESSION['cart'] as $order_item) {

  try {
      $order_id = $_order_id;
      $product_id = $order_item->product_id;

      $q = $db->prepare('INSERT INTO order_items (order_id, product_id)
                          VALUES (:order_id, :product_id)');
      $q->bindValue(':order_id', $order_id);
      $q->bindValue(':product_id', $product_id);
      $q->execute();

  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }
}

_res(200, ['info' => 'order confirmed']);
