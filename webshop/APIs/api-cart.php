<?php
require_once(__DIR__ . '/../private/globals.php');

session_start();

$product_id = $_POST['product_id'];

try {
    $q = $db->prepare('SELECT * FROM products WHERE product_id = :product_id');
    $q->bindValue(':product_id', $product_id);
    $q->execute();

    $product = $q->fetch();

} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

  $_SESSION['cart'][] = $product;
