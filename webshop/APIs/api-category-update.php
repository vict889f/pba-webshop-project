<?php
require_once(__DIR__ . '/../private/globals.php');
session_start();

$statusMsg = '';

// File upload path
$targetDir = __DIR__ . '/../assets/images/category_images/';
$fileName = basename($_FILES["image_file"]["name"]);
$targetFilePath =  $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

// Validate
// if (!isset($_POST['category_name'])) {
//     _res(400, ['info' => 'item_name required', 'error' => __LINE__]);
// }
// if (strlen($_POST['product_name']) < _ITEM_MIN_LEN) {
//     _res(400, ['info' => 'item_name min ' . _ITEM_MIN_LEN . ' characters', 'error' => __LINE__]);
// }
// if (strlen($_POST['product_name']) > _ITEM_MAX_LEN) {
//     _res(400, ['info' => 'item_name min ' . _ITEM_MAX_LEN . ' characters', 'error' => __LINE__]);
// }

if (!isset($_POST["submit"]) && empty($_FILES["image_file"]["name"])) {
    $statusMsg = 'Please select a file to upload.';
    _res(400, ['info' => 'Please select a file to upload.', 'error' => __LINE__]);
}

$allowTypes = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'webp');
if (!in_array(strtolower($fileType), $allowTypes)) {
    $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.';
    _res(400, ['info' => 'Sorry, only JPG, JPEG, PNG, GIF, WebP & PDF files are allowed to upload.', 'error' => __LINE__]);
}

// connect to DB
try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
}

if (move_uploaded_file($_FILES["image_file"]["tmp_name"], $targetFilePath)) {
  try {
      $category_id = $_POST['category_id'];
      $category_name = htmlspecialchars($_POST['category_name'], ENT_QUOTES, 'UTF-8');
      $category_description = htmlspecialchars($_POST['category_description'], ENT_QUOTES, 'UTF-8');

      $q = $db->prepare('UPDATE categories SET category_name = :category_name, category_description = :category_description, image_file = :image_file
      WHERE category_id = :category_id');
      $q->bindValue(':category_id', $category_id);
      $q->bindValue(':category_name', $category_name);
      $q->bindValue(':category_description', $category_description);
      $q->bindValue(':image_file', $fileName);
      $q->execute();

      _res(200, ['info' => 'Update success', 'error' => __LINE__]);
  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }
}
