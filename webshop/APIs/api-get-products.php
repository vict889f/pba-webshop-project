<?php

require_once(__DIR__ . '/../private/globals.php');


try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'test system under maintainance', 'error' => __LINE__]);
}

if (!empty($_GET['category_id']) || !empty($_GET['product_id'])){

  if (!empty($_GET['category_id'])) {

    $category_id = $_GET['category_id'];

    if ($category_id > 0) {

      try {
          $q = $db->prepare('SELECT * FROM products WHERE product_category_id = :category_id');
          $q->bindValue(':category_id', $category_id);
          $q->execute();

          $_products = $q->fetchAll();

      } catch (Exception $ex) {
          _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
      }

      try {
          $q = $db->prepare('SELECT * FROM categories WHERE category_id = :category_id');
          $q->bindValue(':category_id', $category_id);
          $q->execute();

          $_category = $q->fetch();

      } catch (Exception $ex) {
          _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
      }

    } elseif ($category_id == 0) {

      try {
          $q = $db->prepare('SELECT * FROM products');
          $q->execute();

          $_products = $q->fetchAll();

      } catch (Exception $ex) {
          _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
      }
    }
  } elseif (!empty($_GET['product_id'])) {

    $product_id = $_GET['product_id'];

    try {
        $q = $db->prepare('SELECT * FROM products WHERE product_id = :product_id');
        $q->bindValue(':product_id', $product_id);
        $q->execute();

        $_product = $q->fetch();

    } catch (Exception $ex) {
        _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
    }

  }

} else {
  try {
      $q = $db->prepare('SELECT * FROM products');
      $q->execute();

      $_products = $q->fetchAll();

  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }
}
