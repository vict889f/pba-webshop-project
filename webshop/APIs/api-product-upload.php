<?php
require_once(__DIR__ . '/../private/globals.php');

// File upload path
$targetDir = __DIR__ . '/../assets/images/product_images/';
$fileName = basename($_FILES["product_image"]["name"]);
$targetFilePath =  $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

// Validate
if (!isset($_POST['product_name'])) {
    _res(400, ['info' => 'item_name required', 'error' => __LINE__]);
}
if (strlen($_POST['product_name']) < _ITEM_MIN_LEN) {
    _res(400, ['info' => 'item_name min ' . _ITEM_MIN_LEN . ' characters', 'error' => __LINE__]);
}
if (strlen($_POST['product_name']) > _ITEM_MAX_LEN) {
    _res(400, ['info' => 'item_name min ' . _ITEM_MAX_LEN . ' characters', 'error' => __LINE__]);
}

if (!isset($_POST["submit"]) && empty($_FILES["product_image"]["name"])) {
    _res(400, ['info' => 'Please select a file to upload.', 'error' => __LINE__]);
}

$allowTypes = array('jpg', 'png', 'jpeg');
if (!in_array(strtolower($fileType), $allowTypes)) {
    _res(400, ['info' => 'Sorry, only JPG, JPEG, PNG, files are allowed to upload.', 'error' => __LINE__]);
}

// connect to DB
try {
    $db = _db();
} catch (Exception $ex) {
    _res(500, ['info' => 'system under maintenance', 'error' => __LINE__]);
}

if (move_uploaded_file($_FILES["product_image"]["tmp_name"], $targetFilePath)) {
  try {
      // SECURITY UPLOAD:
      $product_name = htmlspecialchars($_POST['product_name'], ENT_QUOTES, 'UTF-8');
      $product_price = htmlspecialchars($_POST['product_price'], ENT_QUOTES, 'UTF-8');
      $product_description_short = htmlspecialchars($_POST['product_description_short'], ENT_QUOTES, 'UTF-8');
      $product_description_long = htmlspecialchars($_POST['product_description_long'], ENT_QUOTES, 'UTF-8');
      $product_category_id = $_POST['product_category_id'];
      $q = $db->prepare('INSERT INTO products (product_name, product_price, product_description_short, product_description_long, product_category_id, product_image)
                        VALUES(:product_name, :product_price, :product_description_short, :product_description_long, :product_category_id, :product_image)');
      $q->bindValue(':product_name', $product_name);
      $q->bindValue(':product_price', $product_price);
      $q->bindValue(':product_description_short', $product_description_short);
      $q->bindValue(':product_description_long', $product_description_long);
      $q->bindValue(':product_category_id', $product_category_id);
      $q->bindValue(':product_image', $fileName);
      $q->execute();

      // Success
      _res(200, ['info' => 'Upload success', 'On line' => __LINE__]);
  } catch (Exception $ex) {
      _res(500, ['info' => 'system under maintainance', 'error' => __LINE__]);
  }
}
