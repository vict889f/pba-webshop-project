<?php
session_start();

require(__DIR__ . '/components/head.php');
require_once(__DIR__ . '/APIs/api-get-products.php');
?>

<body id="home" class="home">
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section id="top-section">
    <div class="img-container">
      <!-- <img src="assets/images/top-image.jpeg" alt="chilli"> -->
      <div class="parallax"></div>
    </div>

    <div class="container">
      <div class="top-inner">
        <h1>Welcome to the Webshop</h1>
        <p>Her skal stå en kortere indbydende tekst der fortæller kort om virksomheden, shoppen eller noget andet.</p>
        <div class="buttons">
          <a class="btn primary-button" href="shop">Go to shop</a>
        </div>
      </div>
    </div>
  </section>
  <section id="product-categories" class="content-section">
    <div class="container">
      <div class="section-title">
        <h2>Shop by category</h2>
        <div class="section-button">
          <a class="btn secondary-button" href="shop">View all products</a>
        </div>
      </div>
      <div class="grid-container">
        <?php
        require_once(__DIR__ . '/APIs/api-get-categories.php');

        $i = 0;

        foreach ($_categories as $item) :
          $i = $i + 1;
          if ($i === 7) :
            break;

          else : ?>

          <div class="grid-item">
            <a href="shop?category_id=<?= $item->category_id ?>">
            <img src="/webshop/assets/images/category_images/<?= $item->image_file ?>" alt="<?= $item->category_name ?>">
            <div class="item-title">
              <div class="title"><?= $item->category_name ?></div>
            </div>
            </a>
          </div>

        <?php  endif;
        endforeach; ?>
      </div>
    </div>
  </section>
  <section id="scroll-section">
    <div class="img-container">
      <div class="parallax middle"></div>
    </div>
  </section>
  <section id="newest-products" class="content-section">
    <div class="container">
      <div class="section-title">
        <h2>Newest products</h2>
        <div class="section-button">
          <a class="btn secondary-button" href="shop">View all products</a>
        </div>
      </div>
      <div class="grid-container product-slider">
        <?php

        $i = 0;

        $_products = array_reverse($_products);
        foreach ($_products as $product) :
          $i = $i + 1;
          if ($i === 7) :
            break;

          else : ?>

          <div class="product-item">
            <form id="product-<?=  $product->product_id ?>" class="product-form" method="post">
              <input name="product_id" type="hidden" value="<?= $product->product_id ?>">
              <a href="product?product_id=<?= $product->product_id ?>">
                <div class="product-image">
                  <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
                </div>
                <div class="product-info">
                  <div class="title">
                    <?= $product->product_name ?>
                  </div>
                  <div class="price"><?= $product->product_price ?> DKK.</div>
                </div>
              </a>
              <button class="btn add-to-cart" tabindex="">Add to cart</button>
            </form>
          </div>

        <?php
          endif;
        endforeach; ?>
      </div>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
