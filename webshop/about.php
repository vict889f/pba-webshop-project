<?php
session_start();

require(__DIR__ . '/components/head.php');

?>

<body>
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section class="sub-top-section">
    <div class="container">
      <h1>About Webshop</h1>
    </div>
  </section>
  <section>
    <div class="container">
      <h2>This is a nice webshop</h2>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
