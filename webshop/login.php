<?php
session_start();

require(__DIR__ . '/components/head.php');

?>

<body id="login-page" class="login-page">
  <?php
    require(__DIR__ . '/components/nav.php');
  ?>
    <section class="form-section">
        <div class="form-wrapper">
            <h2>Sign in</h2>
            <form id="login-form" class="form login-form" method="POST" onsubmit="validate(login); return false">
                <label for="email">Email</label>
                <input name="email" type="text" data-validate="email" tabindex="1" required>
                <div class="label-container">
                    <label for="password">Password</label>
                    <a href="forgot_password"><span class="no-style-button">Forgot your password?</span></a>
                </div>
                <input name="password" type="password" data-validate="str" data-min="6" data-max="20" tabindex="2" required>
                <button class="btn primary-button">Login</button>
            </form>
        </div>
        <p>Dont have an account? <a href='signup.php' type="button">Sign up here</a></p>
    </section>
    <?php
      require(__DIR__ . '/components/footer.php');
    ?>
</body>
