<?php
session_start();

require(__DIR__ . '/components/head.php');

?>

<body>
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section class="sub-top-section">
    <div class="container">
      <h1>Cart</h1>
    </div>
  </section>
  <section id="cart">
    <div class="container">
      <div class="section-title">
        <!-- <h2>Cart</h2> -->
      </div>
      <div class="products-container">
        <?php
        // print_r($_SESSION['cart']);
        if (!empty($_SESSION['cart'])) :
          foreach ($_SESSION['cart'] as $product) : ?>

          <div class="product-item in-cart">
            <div class="product-info">
              <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
              <div class="title">
                <?= $product->product_name ?>
              </div>
            </div>
            <div class="product-right">
              <form id="product-<?=  $product->product_id ?>" class="cart-product-form" method="post">
                <input name="product_id" type="hidden" value="<?= $product->product_id ?>">
                <input name="in_cart" type="hidden" value="<?= $product->product_id ?>">
                <button class="btn remove" tabindex=""></button>
              </form>
              <div class="price"><?= $product->product_price ?> DKK.</div>
            </div>
          </div>
          <hr>
          <?php
        endforeach; ?>

        <div class="total-price">
          <?php if (!empty($_SESSION['cart'])) { ?>
          <div class="amount">Total <?php echo totalPrice(); ?> DKK</div>
          <?php } ?>
        </div>
        <hr>
        <div class="checkout-btn">
          <a class="btn primary-button" href="checkout">Go to checkout</a>
          <br><br>
          <a class="btn small-btn" href="shop">Continue shopping</a>
        </div>
          <?php
        elseif (empty($_SESSION['cart'])) :

          echo "<h2>Cart is empty</h2>";

        endif;  ?>
      </div>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
