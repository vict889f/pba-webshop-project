"use strict";

window.addEventListener('DOMContentLoaded', (event) => {
  console.log('script.js ready');

  const body = document.body;
  if (body.classList.contains('profile-page')) {
    document.getElementById("show-form-one").addEventListener('click', showFormOne);
    document.getElementById("show-form-two").addEventListener('click', showFormTwo);
    event.preventDefault();
  }
  if (body.classList.contains('admin-panel')) {
    document.getElementById("show-form-one").addEventListener('click', showFormOne);
    document.getElementById("show-form-two").addEventListener('click', showFormTwo);
    event.preventDefault();
  }
  if (body.classList.contains('checkout')) {
    document.getElementById("show-form-two").addEventListener('click', showFormTwo);
    let addSelect = document.getElementById("address-select");
    if (addSelect){
      addSelect.addEventListener('click', function(e) {
        addSelect.classList.toggle("active");
        let confirmForm = document.getElementById("confirm-purchase")
        confirmForm.classList.toggle("show");
      });
    }
  }
  if (body.classList.contains('single-product')) {
    document.getElementById("show-form").addEventListener('click', showFormOne);
  }
  if (body.classList.contains('admin')) {
    document.getElementById("show-form").addEventListener('click', showFormOne);
  }
});

const nav = document.getElementById("nav");
if (document.body.contains(nav) && nav.classList.contains('nav-bar')) {

  let scrollpos = window.scrollY

  const nav = document.getElementById("nav");
  const header = document.querySelector("header");
  const scrollChange = 34

  const add_class_on_scroll = () => nav.classList.add("fixed")
  const add_class_on_scroll_top = () => header.classList.add("padding")

  const remove_class_on_scroll_top = () => header.classList.remove("padding")
  const remove_class_on_scroll = () => nav.classList.remove("fixed")

  window.addEventListener('scroll', function() {
    scrollpos = window.scrollY;

    if (scrollpos >= scrollChange) {
      add_class_on_scroll()
      add_class_on_scroll_top()
    }
    else {
      remove_class_on_scroll()
      remove_class_on_scroll_top()
     }
  })
}


const body = document.body;
if (body.classList.contains('home')) {
  const slider = document.querySelector('.product-slider');
  let isDown = false;
  let startX;
  let scrollLeft;

  slider.addEventListener('mousedown', (e) => {
    isDown = true;
    slider.classList.add('active');
    startX = e.pageX - slider.offsetLeft;
    scrollLeft = slider.scrollLeft;
  });
  slider.addEventListener('mouseleave', () => {
    isDown = false;
    slider.classList.remove('active');
  });
  slider.addEventListener('mouseup', () => {
    isDown = false;
    slider.classList.remove('active');
  });
  slider.addEventListener('mousemove', (e) => {
    if (!isDown) return;
    e.preventDefault();
    const x = e.pageX - slider.offsetLeft;
    const walk = (x - startX) * 1; //scroll-fast
    slider.scrollLeft = scrollLeft - walk;
  });

}

function showFormOne() {
    console.log('clicked');
    document.querySelector("#popupOne").style.display = "block";
    document.body.style.overflow = 'hidden';
    document.querySelector("#popupOne .luk").addEventListener("click", closeItem);

    function closeItem() {
        document.querySelector("#popupOne").style.display = "none";
        document.body.style.overflow = 'unset';
    }
}

function showFormTwo() {
    console.log('clicked');
    document.querySelector("#popupTwo").style.display = "block";
    document.body.style.overflow = 'hidden';

    const body = document.body;
    if (body.classList.contains('checkout') && document.getElementById("address-select")) {
      let element = document.getElementById("address-select");
      element.classList.remove("active");
    }

    document.querySelector("#popupTwo .luk").addEventListener("click", closeItem);

    function closeItem() {
        document.querySelector("#popupTwo").style.display = "none";
        document.body.style.overflow = 'unset';
    }
}
