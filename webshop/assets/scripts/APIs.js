"use strict";

window.addEventListener('DOMContentLoaded', (event) => {
    console.log('API.js ready');
    event.preventDefault();

    const div = document.body;
    if (div.classList.contains('admin-panel')) {
      console.log('admin-panel');
      document.getElementById("product-form").addEventListener('submit', uploadProduct);
      document.getElementById("category-form").addEventListener('submit', uploadCategory);
    }
    if (div.classList.contains('single-product')) {
      document.getElementById("update-product-form").addEventListener('submit', updateProduct);
      console.log('single-product');
    }
    if (div.classList.contains('admin')) {
      document.getElementById("update-category-form").addEventListener('submit', updateCategory);
      console.log('shop');
    }
    if (div.classList.contains('signup-page')) {
      // document.getElementById("signup-form").addEventListener('submit', validate(signUp));
      console.log('signup-page');
    }
    if (div.classList.contains('login-page')) {
      // document.getElementById("login-form").addEventListener('submit', validate(login));
      console.log('login-page');
    }
    if (div.classList.contains('forgot-password')) {
      document.getElementById("forgot-password-form").addEventListener('submit', sendEmail);
      console.log('forgot-password');
    }
    if (div.classList.contains('change-password')) {
      document.getElementById("new-password-form").addEventListener('submit', changePassword);
      console.log('change-password');
    }
    if (div.classList.contains('profile-page')) {
      document.getElementById("user-form").addEventListener('submit', updateUser);

      if (document.getElementById("update-info-form")) {
        document.getElementById("update-info-form").addEventListener('submit', updateUserInfo);
      }
      if (document.getElementById("user-info-form")) {
        document.getElementById("user-info-form").addEventListener('submit', uploadUserInfo);
      }
      console.log('profile-page');
    }
    if (div.classList.contains('checkout')) {
      if (document.getElementById("update-info-form")) {
        document.getElementById("update-info-form").addEventListener('submit', updateUserInfo);
      }
      if (document.getElementById("user-info-form")) {
        document.getElementById("user-info-form").addEventListener('submit', uploadUserInfo);
      }
      document.getElementById("confirm-purchase").addEventListener('submit', placeOrder);
      console.log('checkout');
    }

    let productForm = document.querySelectorAll(".product-form");
      productForm.forEach(form => {
        form.addEventListener('submit', addToCart);
      });
    let cartProductForm = document.querySelectorAll(".cart-product-form");
      cartProductForm.forEach(form => {
        form.addEventListener('submit', removeFromCart);
      });

    let deleteProductForm = document.querySelectorAll(".remove-product-form");
      deleteProductForm.forEach(form => {
        form.addEventListener('submit', deleteProduct);
      });
    let deleteCategoryForm = document.querySelectorAll(".remove-category-form");
      deleteCategoryForm.forEach(form => {
        form.addEventListener('submit', deleteCategory);
      });
});


// --------------------------- Product upload ---------------------------

async function uploadProduct(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-product-upload", {
        method: "POST",
        body: new FormData(document.getElementById('product-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Product update ---------------------------

async function updateProduct(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-product-update", {
        method: "POST",
        body: new FormData(document.getElementById('update-product-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Product delete  ---------------------------

async function deleteProduct(event) {
    event.preventDefault();

    console.log(event);
    console.log("delete");
    const conn = await
    fetch("APIs/api-product-delete", {
        method: "POST",
        body: new FormData(event.target)
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("product deleted");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Category upload ---------------------------

async function uploadCategory(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-category-upload", {
        method: "POST",
        body: new FormData(document.getElementById('category-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Category update ---------------------------

async function updateCategory(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-category-update", {
        method: "POST",
        body: new FormData(document.getElementById('update-category-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Category delete  ---------------------------

async function deleteCategory(event) {
    event.preventDefault();

    console.log(event);
    console.log("delete");
    const conn = await
    fetch("APIs/api-category-delete", {
        method: "POST",
        body: new FormData(event.target)
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("category deleted");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- add to cart ---------------------------

async function addToCart(event) {
    event.preventDefault();

    console.log(event);
    const conn = await
    fetch("APIs/api-cart", {
        method: "POST",
        body: new FormData(event.target)
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("added to cart");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- remove from cart ---------------------------

async function removeFromCart(event) {
    event.preventDefault();

    console.log(event);
    console.log("remove");
    const conn = await
    fetch("APIs/api-cart-remove", {
        method: "POST",
        body: new FormData(event.target)
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("removed from cart");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- LOGIN ---------------------------
async function login(event) {
  // event.preventDefault();
  console.log("clicked")
  const conn = await
  fetch("APIs/api-login", {
      method: "POST",
      body: new FormData(document.getElementById('login-form'))
  })
  const res = await conn.text()
  // console.log(res)

  if (conn.ok) {
      console.log("okay");
      location.href = "index";
  }else {
    alert(res)
  }
}

// --------------------------- SIGNUP ---------------------------

async function signUp(event) {
    // event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-signup", {
        method: "POST",
        body: new FormData(document.getElementById('signup-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        location.href = "signup_ok";
    }else {
      alert(res)
    }
}

// --------------------------- Udate user ---------------------------

async function updateUser(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-user-update", {
        method: "POST",
        body: new FormData(document.getElementById('user-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Upload user info ---------------------------

async function uploadUserInfo(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-user-info", {
        method: "POST",
        body: new FormData(document.getElementById('user-info-form'))
    })
    const res = await conn.text()
    console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Udate user info ---------------------------

async function updateUserInfo(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-user-info-update", {
        method: "POST",
        body: new FormData(document.getElementById('update-info-form'))
    })
    const res = await conn.text()
    // console.log(res)
    if (conn.ok) {
        console.log("okay");
        window.location.reload()
    }else {
      alert(res)
    }
}

// --------------------------- Forgot Password: Send email ---------------------------

async function sendEmail(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-password-email.php", {
        method: "POST",
        body: new FormData(document.getElementById('forgot-password-form'))
    })
    const res = await conn.json();
    // console.log(res)
    if (conn.ok) {
        console.log("email sent")
        document.querySelector(".message").insertAdjacentHTML("afterbegin", `<p>Email has been sent - check your email and follow the link<p>`)
    }else {
      alert(res)
    }
}

// --------------------------- Forgot Password: Change password ---------------------------

async function changePassword(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-password-update.php", {
        method: "POST",
        body: new FormData(document.getElementById('new-password-form'))
    })

    const res = await conn.json();
    // console.log(res)
    if (conn.ok) {
        console.log("ok - password updated")
        alert("Your password has been updated")
        location.href = "login"
    }else {
      alert(res)
    }
}

// --------------------------- Order ---------------------------

async function placeOrder(event) {
    event.preventDefault();
    console.log("clicked")
    const conn = await
    fetch("APIs/api-order.php", {
        method: "POST",
        body: new FormData(document.getElementById('confirm-purchase'))
    })

    const res = await conn.text();
    // console.log(res)
    if (conn.ok) {
        console.log("ok")
        alert("Your purchase has gone through")
        location.href = "APIs/api-order-completed"
    }else {
      alert(res)
    }
}
