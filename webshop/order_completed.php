<?php
session_start();

require(__DIR__ . '/components/head.php');

if ($_SESSION['user'] == false ) {
  header("Location: login");
}
if (empty($_SESSION['order'])) {
  header("Location: shop");
}
?>

<body>
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section class="sub-top-section">
    <div class="container">
      <h1>Order confirmed</h1>
    </div>
  </section>
  <section id="cart">
    <div class="container">
      <div class="section-title">
        <h2>Order confirmation has been sendt to your email</h2>
      </div>
      <div class="products-container">
        <?php
        // print_r($_SESSION['order']);
        if (!empty($_SESSION['order'])) :
          foreach ($_SESSION['order'][0] as $product) : ?>

          <div class="product-item in-cart">
            <div class="product-info">
              <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
              <div class="title">
                <?= $product->product_name ?>
              </div>
            </div>
            <div class="product-right">
              <form id="product-<?=  $product->product_id ?>" class="cart-product-form" method="post">
              </form>
              <div class="price"><?= $product->product_price ?> DKK.</div>
            </div>
          </div>
          <hr>
          <?php
        endforeach; ?>

        <div class="total-price">
          <?php if (!empty($_SESSION['order'])) { ?>
          <div class="amount">Total <?php echo totalPrice(); ?> DKK</div>
          <?php } ?>
        </div>
          <?php
        elseif (empty($_SESSION['order'])) :

          echo "<h2>Error: why are you here?</h2>";

        endif;  ?>
      </div>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
