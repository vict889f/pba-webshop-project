<?php
session_start();

if ( empty($_SESSION['user']) ) {
  header("Location: login");
  exit();
}

if ( empty($_SESSION['cart']) ) {
  header("Location: shop");
  exit();
}

require(__DIR__ . '/components/head.php');

require_once(__DIR__ . '/APIs/api-get-users.php');
?>

<body id="checkout" class="checkout">
  <?php
  require(__DIR__ . '/components/nav.php');
  ?>
  <section class="sub-top-section">
    <div class="container">
      <h1>Checkout</h1>
    </div>
  </section>
  <section id="adress">
    <div class="container">
      <div class="">
        <h3>Use saved billing address</h3>
        <br>
        <div class="address-wrap">
          <a id="<?= !($_user_information) ? 'show-form-two' : 'address-select'; ?>" class="btn address">
            <div class="adress-inner <?= !($_user_information) ? 'add' : ''; ?>">
              <?php if ($_user_information): ?>
                <?= $_user->user_name . " " . $_user->last_name ?>
                <br>
                <?= $_user_information->address ?>
                <br>
                <?= $_user_information->city ?>
                <br>
                <?= $_user_information->zip ?>
                <br>
                <?= $_user_information->country ?>
              <?php else : ?>
                <div class="add-icon">
                  <p>Add billing information</p>
                </div>
              <?php endif; ?>
            </div>
          </a>
          <?php if ($_user_information): ?>
            <a id="show-form-two" class="btn address">
              <div class="adress-inner add">
                <div class="add-icon">
                  <p>Use different billing information</p>
                </div>
              </div>
            </a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div id="popupTwo" class="popup" data-theme="">
        <section class="indhold">
          <button class="luk"></button>
          <div class="form-wrapper">
              <h2>Shipping information</h2>
              <form id="<?= ($_user_information) ? 'update-info-form' : 'user-info-form'; ?>" class="form user-info-form" method="POST">
                  <input name="user_id" type="hidden" value="<?= $_user->user_id ?>">
                  <label for="address">Address</label>
                  <input type="text" name="address" value="<?= ($_user_information) ? $_user_information->address : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="1" required>
                  <label for="city">City</label>
                  <input type="text" name="city" value="<?= ($_user_information) ? $_user_information->city : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="2" required>
                  <label for="zip">Zip code</label>
                  <input type="text" name="zip" value="<?= ($_user_information) ? $_user_information->zip : ''; ?>" data-validate="str" tabindex="3" required>
                  <label for="country">Country</label>
                  <input type="text" name="country" value="<?= ($_user_information) ? $_user_information->country : ''; ?>" data-validate="str" data-min="2" data-max="20" tabindex="2" required>
                  <label for="preferred_payment">Preferred payment method</label>
                  <div class="radio-btn">
                    <input type="radio" id="paypall" name="payment" value="1" <?= ($_user_information) ? (($_user_information->preferred_payment == 1) ? 'checked' : '') : ''; ?>>
                    <label for="paypall">Paypall</label>
                  </div>
                  <div class="radio-btn">
                    <input type="radio" id="mobilepay" name="payment" value="2" <?= ($_user_information) ? (($_user_information->preferred_payment == 2) ? 'checked' : '') : ''; ?>>
                    <label for="mobilepay">MobilePay</label>
                  </div>
                  <div class="radio-btn">
                    <input type="radio" id="card" name="payment" value="3" <?= ($_user_information) ? (($_user_information->preferred_payment == 3) ? 'checked' : '') : ''; ?>>
                    <label for="card">Card</label>
                  </div>
                  <br>
                  <button class="btn primary-button" tabindex="6">Confirm</button>
              </form>
          </div>
        </section>
  </div>
</section>
  <section class="payment-info">
    <div class="container">
      <h3>Payment information</h3><br>
      <?php if ($_user_information): ?>
        <?php if ($_user_information->preferred_payment == 1) : ?>
          <h4>Pay with paypal</h4>
          <img src="assets/images/paypal.png" alt="paypal">
        <?php elseif ($_user_information->preferred_payment == 2) :?>
          <h4>Pay with MobilePay</h4>
          <img src="assets/images/mobilepay.png" alt="paypal">
        <?php elseif ($_user_information->preferred_payment == 3) :?>
          <h4>Pay with card</h4>
          <img src="assets/images/card.png" alt="paypal">
        <?php endif; ?>
      <?php else : ?>
        <h4>Payment method not defined</h4>
      <?php endif; ?>
    </div>
  </section>
  <section id="cart">
    <div class="container">
      <h3>Order review</h3><br>
      <div class="products-container">
        <?php
        if (!empty($_SESSION['cart'])) :
          foreach ($_SESSION['cart'] as $product) : ?>

          <div class="product-item in-cart">
            <div class="product-info">
              <img src="/webshop/assets/images/product_images/<?= $product->product_image ?>" alt="<?= $product->product_name ?>">
              <div class="title">
                <?= $product->product_name ?>
              </div>
            </div>
            <div class="product-right">
              <form id="product-<?=  $product->product_id ?>" class="cart-product-form" method="post">
                <input name="product_id" type="hidden" value="<?= $product->product_id ?>">
                <input name="in_cart" type="hidden" value="<?= $product->product_id ?>">
                <button class="btn remove" tabindex=""></button>
              </form>
              <div class="price"><?= $product->product_price ?> DKK.</div>
            </div>
          </div>
          <hr>
          <?php
        endforeach; ?>

        <div class="total-price">
          <?php if (!empty($_SESSION['cart'])) { ?>
            <?php $total_price = totalPrice() ?>
          <div class="amount">Total <?= $total_price ?> DKK</div>
          <?php } ?>
        </div>
        <hr>
        <div class="checkout-btn">
          <form id="confirm-purchase" class="form hide" method="post">
            <input name="user_id" type="hidden" value="<?= $_user->user_id ?>">
            <input name="total_price" type="hidden" value="<?= $total_price ?>">
            <button class="btn primary-button" tabindex="">Confirm purchase</button>
          </form>
        </div>
          <?php
        elseif (empty($_SESSION['cart'])) :

          echo "<h2>Error: no items</h2>";

        endif;  ?>
      </div>
    </div>
  </section>
  <?php
  require(__DIR__ . '/components/footer.php');
  ?>
</body>
